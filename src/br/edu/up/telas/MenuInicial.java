package br.edu.up.telas;

import java.util.Scanner;

import br.edu.up.modelos.Pedido;
import br.edu.up.controles.ControleDePedidos;

public class MenuInicial {

    private Scanner leitor = new Scanner(System.in);

    public void mostrarMenu() {

        System.out.println();
        System.out.println("-----------------");
        System.out.println("  MENU INICIAL");
        System.out.println("-----------------");
        System.out.println();
        System.out.println("Digite o número da opção:");
        System.out.println();

        System.out.println("1. Cadastrar funcionários");
        System.out.println("2. Cadastrar cardápios");
        System.out.println("...");
        System.out.println("5. Controlar pedidos");

        int opcao = leitor.nextInt();

        switch (opcao) {
            case 1:
                break;
            case 5: {
                mostrarMenuPedidos();
                break;
            }
            default:
                break;
        }

    }

    public void mostrarMenuPedidos() {

        System.out.println();
        System.out.println("-----------------");
        System.out.println("  MENU PEDIDOS");
        System.out.println("-----------------");
        System.out.println();
        System.out.println("Digite o número da opção:");
        System.out.println();

        System.out.println("1. Incluir");
        System.out.println("2. Alterar");
        System.out.println("3. Listar");
        System.out.println("4. Excluir");
        System.out.println("5. Voltar");

        int opcao = leitor.nextInt();

        ControleDePedidos controle = new ControleDePedidos();

        switch (opcao) {
            case 1:
                break;
            case 3: {
                // listar os pedidos
                Pedido[] pedidos = controle.getPedidos();

                System.out.println("-----Pedidos----");
                for (int i = 0; i < pedidos.length; i++) {
                    Pedido pedido = pedidos[i];
                    // String str = "Número: " + pedido.getNumero() + " Data: " + pedido.getData();

                    System.out.println(pedido);
                }
                break;
            }
            case 5:
            default:
                // voltar
                break;
        }

    }

}
