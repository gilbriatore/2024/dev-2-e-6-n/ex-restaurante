package br.edu.up.modelos;

public class Pedido {

    // O pedido tem um

    private String numero, data;

    Cliente cliente;
    Garcom garcom;
    Item[] itens;

    public Pedido() {
    }

    public Pedido(String numero, String data) {
        this.numero = numero;
        this.data = data;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Número: " + numero + " Data: " + data;
    }
}
